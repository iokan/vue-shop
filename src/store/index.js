import { createStore } from 'vuex'
import product from './modules/product'
import navbar from './modules/navbar'
import cart from './modules/cart'
import tags from './modules/tags'

export default createStore({
  modules: {
    product, navbar, cart, tags
  }
})
