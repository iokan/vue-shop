import axios from 'axios'

export default {
  state: {
    cart: [],
    cartStatus: false
  },
  mutations: {
    SET_TO_CART (state, product) {
      if (state.cart) {
        let isProductExists = false
        state.cart.map(function (item) {
          if (item.id === product.id) {
            isProductExists = true
            item.count++
          }
        })
        if (!isProductExists) {
          product.count = 1
          state.cart.push(product)
        }
      } else {
        product.count = 1
        state.cart.push(product)
      }
    },
    PLUS_TO_CART (state, product) {
      state.cart.find(item => item.id === product.id).count++
    },
    MINUS_TO_CART (state, product) {
      if (state.cart.find(item => item.id === product.id).count > 1) {
        state.cart.find(item => item.id === product.id).count--
      } else {
        const index = state.cart.findIndex(item => item.id === product.id)
        state.cart.splice(index, 1)
      }
    },
    REMOVE_FROM_CART (state, index) {
      state.cart.splice(index, 1)
    },
    CART_STATUS_SWITCH (state) {
      state.cartStatus = !state.cartStatus
    },
    PUT_CART_FROM_STORAGE (state, products) {
      state.cart = products
    },
    CLEAR_CART (state) {
      state.cart = []
    }
  },
  actions: {
    ADD_TO_CART ({ commit }, product) {
      commit('SET_TO_CART', product)
    },
    PLUS_TO_CART ({ commit }, product) {
      commit('PLUS_TO_CART', product)
    },
    MINUS_TO_CART ({ commit }, product) {
      commit('MINUS_TO_CART', product)
    },
    DELETE_FROM_CART ({ commit }, index) {
      commit('REMOVE_FROM_CART', index)
    },
    CART_STATUS_SWITCHER ({ commit }) {
      commit('CART_STATUS_SWITCH')
    },
    CART_FROM_STORAGE ({ commit }, cart) {
      commit('PUT_CART_FROM_STORAGE', cart)
    },
    ORDER_SUBMIT ({ commit }, cart) {
      return axios
        .post('http://localhost:3000/api/orders', {
          order: cart
        })
        .then((response) => {
          commit('CLEAR_CART')
          console.log(response)
          return response
        })
        .catch((error) => {
          console.log(error)
          return error
        })
    }
  },
  getters: {
    CART (state) {
      return state.cart
    },
    CART_STATUS (state) {
      return state.cartStatus
    }
  }
}
