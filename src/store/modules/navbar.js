export default {
  state: {
    navbarStatus: false
  },
  mutations: {
    NAVBAR_STATUS_SWITCH (state) {
      state.navbarStatus = !state.navbarStatus
    }
  },
  actions: {
    NAVBAR_STATUS_SWITCHER ({ commit }) {
      commit('NAVBAR_STATUS_SWITCH')
    }
  },
  getters: {
    NAVBAR_STATUS (state) {
      return state.navbarStatus
    }
  }
}
