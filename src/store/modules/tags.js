export default {
  state: {
    tags: [],
    selectedTags: []
  },
  mutations: {
    UPDATE_TAGS (state, products) {
      const tags = []
      products.map(function (item) {
        const arrProductTags = item.tag.split(' ')
        tags.push(arrProductTags[0])
      })
      products.map(function (item) {
        const arrProductTags = item.tag.split(' ')
        arrProductTags.forEach(function (tag) {
          if (tags.indexOf(tag) !== -1) {
            return false
          } else {
            tags.push(tag)
          }
        })
      })
      const uniqueTags = tags.filter(function (item, position) {
        return tags.indexOf(item) === position
      })
      console.log(uniqueTags)
      state.tags = Object.assign([], uniqueTags)
    },
    UPDATE_SELECTED_TAGS (state, tag) {
      state.selectedTags.length = 0
      state.selectedTags.push(tag)
    },
    REMOVE_LAST_SELECTED_TAG (state) {
      state.selectedTags.pop()
    }
  },
  actions: {
    SELECT_TAG ({ commit }, tag) {
      commit('UPDATE_SELECTED_TAGS', tag)
    },
    REMOVE_TAG ({ commit }) {
      commit('REMOVE_LAST_SELECTED_TAG')
    }
  },
  getters: {
    TAGS (state) {
      return state.tags
    },
    SELECTED_TAGS (state) {
      return state.selectedTags
    }
  }
}
