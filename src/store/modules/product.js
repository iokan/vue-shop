import axios from 'axios'
export default {
  state: {
    products: []
  },
  mutations: {
    UPDATE_PRODUCTS (state, products) {
      const sortedProducts = []
      products.map(function (item) {
        if (item.amount !== 0) {
          item.category === products[0].category ? sortedProducts.unshift(item) : sortedProducts.push(item)
        }
      })
      // Положить товары, которых нет в наличии, в конец массива
      products.map(function (item) {
        if (item.amount === 0) {
          sortedProducts.push(item)
        }
      })
      state.products = sortedProducts
    }
  },
  actions: {
    GET_PRODUCTS ({ commit }) {
      return axios
        .get('http://localhost:3000/api/products')
        .then((response) => {
          commit('UPDATE_PRODUCTS', response.data)
          commit('UPDATE_TAGS', response.data)
          return response
        })
        .catch((error) => {
          console.log(error)
          return error
        })
    }
  },
  getters: {
    PRODUCTS (state) {
      return state.products
    }
  }
}
